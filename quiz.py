#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import splitmp3
import s3uploader
import json
import requests
import os.path
from os import listdir
import sys
import random
import shutil

DEBUG = True # set False for use in production mode

# default settings for quiz
DEFAULT_CATEGORY_ID = '5541538603991040'
DEFAULT_LEVEL = 1
DEFAULT_APROVE = True
DEFAULT_TYPE = 3 # 1, 2, 3 for text, image, audio

PART_LENGTH = 15 # seconds per part

######################## program's settings ##################################
SERVLET_URL = ''
BUCKET = ''
if DEBUG:
    SERVLET_URL = 'http://127.0.0.1:8888/autoInsertQuiz'
    #SERVLET_URL = 'http://admin.heroandtn3-quiz.appspot.com/autoInsertQuiz'
    BUCKET = 'For_Testing'
else:
    SERVLET_URL = 'http://admin.who-knows-more.appspot.com/autoInsertQuiz'
    BUCKET = 'WKM'

BASE_URL = 'http://s3.amazonaws.com/' + BUCKET

class Quiz():
    """
    Simple quiz for audio type.
    """

    def __init__(self):
        self.questions_text = []
        self.questions_image = [] # list of image's url
        self.questions_audio = [] # list of sound's url

        self.answers_text = []
        self.answers_image = []
        self.answers_audio = []

        self.level = DEFAULT_LEVEL
        self.category_id = DEFAULT_CATEGORY_ID
        self.aprove = DEFAULT_APROVE
        self.type = DEFAULT_TYPE

    def to_json(self):
        """
        """
        q = {
            'questions_text': self.questions_text,
            'questions_image': self.questions_image,
            'questions_audio': self.questions_audio,
            'answers_text': self.answers_text,
            'answers_image': self.answers_image,
            'answers_audio': self.answers_audio,
            'level': self.level,
            'category_id': self.category_id,
            'aprove': self.aprove,
            'type': self.type
        }
        return json.dumps(q)

class Song():
    """
    Contains song informations.
    """
    
    def __init__(self, name='', artist='', composer='', urls=[], filepath=''):
        self.name = name
        self.artist = artist
        self.composer = composer
        self.urls = urls
        self.filepath = filepath

def parse_song(filepath):
    """
    Parse song from file name.
    """
    filename = filepath.split('/')[-1]
    fields = filename[:filename.rfind('.')].split('_')
    name = fields[0]
    artist = fields[1]
    composer = fields[2]

    parts = splitmp3.split(
        filepath, 
        part_length=PART_LENGTH, 
        output_dir=os.path.join('output', filename),
        padding=0)
    return Song(
        name = name,
        artist = artist,
        composer = composer,
        urls = parts,
        filepath = filepath
    )

def parse_quiz(song, songs):
    """
    Parse list of quizs from song.

    Return: list of quizs use same song.
    """
    quizs = []

    # quiz by name
    quiz = Quiz()
    quizs.append(quiz)

    quiz.questions_audio = song.urls
    quiz.questions_text.append('Bài hát bạn đang nghe tên là gì?')

    quiz.answers_text.append(song.name)
    while len(quiz.answers_text) < 4:
        i = random.randint(0, len(songs) - 1)
        name = songs[i].name
        if name not in quiz.answers_text:
            quiz.answers_text.append(name)

    # quiz by artist
    quiz = Quiz()
    quizs.append(quiz)

    quiz.questions_audio = song.urls
    quiz.questions_text.append('Bài hát bạn đang nghe do ai thể hiện?')

    quiz.answers_text.append(song.artist)
    while len(quiz.answers_text) < 4:
        i = random.randint(0, len(songs) - 1)
        artist = songs[i].artist
        if artist not in quiz.answers_text:
            quiz.answers_text.append(artist)

    # quiz by composer
    quiz = Quiz()
    quizs.append(quiz)

    quiz.questions_audio = song.urls
    quiz.questions_text.append('Bài hát bạn đang nghe do ai sáng tác?')

    quiz.answers_text.append(song.composer)
    while len(quiz.answers_text) < 4:
        i = random.randint(0, len(songs) - 1)
        composer = songs[i].composer
        if composer not in quiz.answers_text:
            quiz.answers_text.append(composer)

    return quizs

def upload_quiz(quizs):
    """
    Upload quiz

    Return: quiz's id.
    """
    print 'Uploading', len(quizs), 'quiz...'
    data = []
    for quiz in quizs:
        data.append(quiz.to_json())

    resp = requests.post(SERVLET_URL, params={
        'action': 'upload',
        'base_url': BASE_URL
        },
        data=str(data))
    qid = resp.text.strip()
    print 'Upload quizs successful'
    return qid

def upload_sounds(paths, qid):
    """
    Upload all files in parts to S3.
    """
    print 'Uploading', len(paths), 'audio files'
    for path in paths:
        order = path.split('.')[-1]
        new_name = qid + '.' + order + '.mp3'
        rs = s3uploader.upload(path, new_name, bucket_name=BUCKET)
        if rs == True:
            print 'Upload', path, 'successful'
        else:
            print 'Upload', path, 'failed'

def del_output_folder():
    shutil.rmtree('output')

if __name__ == '__main__':
    #song = parse_song('Em trong mắt tôi_Nguyễn Đức Cường_Ngô Doãn Sang.mp3')
    #quizs = parse_quiz(song)
    #qid = upload_quiz(quizs)
    #upload_sounds(song.urls, qid)

    folder = sys.argv[1]
    if folder[-1] == '/':
        folder = folder[:-1]

    file_list = [f for f in listdir(folder) \
        if os.path.isfile(os.path.join(folder, f)) and f.endswith('.mp3')]
    print 'Found', len(file_list), 'files, parsing to song...'

    songs = []
    for filename in file_list:
        songs.append(parse_song(os.path.join(folder, filename)))
    print ('Parse files to songs successful,'
        ' press Enter to start upload or Ctr+C to cancel.')
    try:
        raw_input()
    except KeyboardInterrupt:
        del_output_folder()
        print 'Bye!'
        sys.exit(0)

    quizs = []
    for song in songs:
        print 'Parsing', song.name, '...'
        qs = parse_quiz(song, songs)
        quizs.extend(qs)
        print 'Uploading', song.name, 'to datastore...'
        qid = upload_quiz(qs)
        print 'Uploading', song.name, 'to S3...'
        upload_sounds(song.urls, qid)
        print "Uploaded '{0}'".format(song.name)
    print 'Uploaded total', len(quizs), 'quizs successful!'

    del_output = raw_input('Delete output directory? (Y/N) ')
    if del_output == 'Y' or del_output == 'y':
        del_output_folder()
    print 'Well done, Bye!'