#!/usr/bin/env python

from mutagen.mp3 import MP3
import os.path
from os import makedirs

PADDING = 10 # split from 10th seconds

def __suggest_extension__(filename, default='mp3'):
    """
    Suggest a extension for file

    Return: extension of file without dot.
    Example: mp3, wmv,...
    """
    if '.' in filename:
        return filename.split('.')[-1]
    else:
        return default


def split(filename, part_length=10, output_dir='output', padding=PADDING):
    """
    Split MP3 file into parts with specify length

    Return: list of parts as file's name.
    """
    audio = MP3(filename)
    byte_rate = audio.info.bitrate / 8 # bytes per seconds
    
    chunk_size = byte_rate * part_length
    parts = []

    with open(filename) as f:
        data = f.read()
        file_size = len(data)

        if not os.path.isdir(output_dir):
            makedirs(output_dir)

        start = padding * byte_rate
        end = start

        while end < file_size:
            end = start + chunk_size
            part_data = data[start:end]
            part_name = os.path.join(
                output_dir, filename.split('/')[-1] + '.' + str(len(parts)))

            with open(part_name, 'w') as fp:
                fp.write(part_data)
                parts.append(part_name)
            start = end

    if len(parts) == 0:
        return [os.path.join(output_dir, filename + '.0')]
    if len(parts) > 1:
        return parts[:-1] # remove last part
    else:
        return parts

if __name__ == '__main__':
    print(split('sound.mp3', 30))