#!/usr/bin/env python2

from boto.s3.connection import S3Connection
from boto.s3.connection import OrdinaryCallingFormat
from boto.s3.key import Key
from StringIO import StringIO

### Default setting
BUCKET_NAME = 'For_Testing'

###

conn = S3Connection('AKIAJJNKW7VI7KQ6HQNA', 'tHzzWtlhS+BACOZ1OeIprMBN0YQ37R663G3LhuBf', calling_format=OrdinaryCallingFormat())

def upload(filename, new_name, bucket_name=BUCKET_NAME):
    """
    Upload filename to S3 storage

    Return: number of bytes written to S3 if successful
    Error if not. 
    """
    bucket = conn.get_bucket(bucket_name)
    k = Key(bucket)
    k.key = new_name
    if k.exists():
        print('Error: filename exists! Byte!')
        return False
    return (k.set_contents_from_filename(filename) > 0)

if __name__ == '__main__':
    print(upload('output/001.mp3/001.mp3.0', '0001.mp3.0'))
