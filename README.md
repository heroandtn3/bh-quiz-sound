###Requirements

python 2 with modules: `requests`, `boto`, `mutagen`

###Sound format

`<Name>_<Artist>_<Composer>.mp3`

Note: sound folder should have at lest 4 files.

###Setting

IMPORTANT: Edit setting in file `quiz.py`. Set `DEBUG` to `False` to use
in production mode

Other setting should be consider:
```
DEFAULT_CATEGORY_ID = '4960996464525312'
DEFAULT_LEVEL = 1
DEFAULT_APROVE = True
DEFAULT_TYPE = 3 # 1, 2, 3 for text, image, audio

PART_LENGTH = 15 # seconds per part
```

###Usage

`$ python2 quiz.py <sound folder>`

###Example

Sound folder looks like this:

```
$ ls sounds/
Em khong quay ve_Hoang Ton_Hoang Ton.mp3
Huong ngoc lan_My Linh_Khong biet ten.mp3
Minh yeu nhau di_Bich Phuong_Tien Cookie.mp3
Ngay ay se den_Ho Quang Hieu_Ho Quang Hieu.mp3
Tinh yeu mau nang_Doan Thuy Trang_Pham Thanh Ha.mp3
```

Run command:
```
python2 quiz.py sounds/
Found 5 files, parsing to song...
Parse files to songs successful, converting to quizs and upload...
Parsing Huong ngoc lan ...
Uploading Huong ngoc lan to datastore...
Uploading 3 quiz...
Upload quizs successful
Uploading Huong ngoc lan to S3...
Uploading 23 audio files
Upload output/Huong ngoc lan_My Linh_Khong biet ten.mp3/Huong ngoc lan_My Linh_Khong biet ten.mp3.0 successful
Upload output/Huong ngoc lan_My Linh_Khong biet ten.mp3/Huong ngoc lan_My Linh_Khong biet ten.mp3.1 successful
Upload output/Huong ngoc lan_My Linh_Khong biet ten.mp3/Huong ngoc lan_My Linh_Khong biet ten.mp3.2 successful
```
